import {Component, OnDestroy, OnInit} from '@angular/core';
import {EarthquakeService} from '../services/earthquake.service';
import {EarthquakeResponse} from '../models/earthquakeResponse';
import {Subject} from 'rxjs';


@Component({
  selector: 'app-vertical-bar-chart',
  templateUrl: './vertical-bar-chart.component.html',
  styleUrls: ['./vertical-bar-chart.component.css']
})
export class VerticalBarChartComponent implements OnInit, OnDestroy {
  private destroyed$ = new Subject();
  data: EarthquakeResponse;
  public chartData = [];
  view: any[] = [1000, 700];

  // options for the chart
  showXAxis = true;
  showYAxis = true;
  gradient = false;
  showLegend = false;
  showXAxisLabel = true;
  xAxisLabel = 'Country';
  showYAxisLabel = true;
  yAxisLabel = 'Sales';
  timeline = true;


  colorScheme = {
    domain: ['#9370DB', '#87CEFA', '#FA8072', '#FF7F50', '#90EE90', '#9370DB']
  };

  constructor(private readonly earthquakeService: EarthquakeService) {
  }

  ngOnInit() {
    this.earthquakeService.getEarthquakeData().subscribe(data => {
      this.data = data;
      this.generateChartData();
    });

    this.earthquakeService.refreshEarthquakeData()
      .subscribe();
  }

  generateChartData() {
    for (const p of this.data.properties) {
      const temp = {
        name: p.place,
        value: p.mag
      };
      this.chartData.push(temp);
    }
  }

  ngOnDestroy() {
    this.destroyed$.next();
    this.destroyed$.complete();
  }

}
