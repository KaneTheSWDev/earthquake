import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';

import {HttpClientModule} from '@angular/common/http';
import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {NgxChartsModule} from '@swimlane/ngx-charts';
import {VerticalBarChartComponent} from './vertical-bar-chart/vertical-bar-chart.component';
import {AgmCoreModule} from '@agm/core';
import {MapComponent} from './map/map.component';
import {MatButtonModule, MatIconModule, MatSidenavModule, MatToolbarModule, MatTableModule, MatPaginatorModule} from '@angular/material';
import { MainNavComponent } from './main-nav/main-nav.component';
import { LayoutModule } from '@angular/cdk/layout';
import { MatListModule } from '@angular/material/list';
import { LinearGaugeChartComponent } from './linear-gauge-chart/linear-gauge-chart.component';
import { PieChartComponent } from './pie-chart/pie-chart.component';
import { DataTableComponent } from './data-table/data-table.component';

@NgModule({
  declarations: [
    AppComponent,
    VerticalBarChartComponent,
    MapComponent,
    MainNavComponent,
    LinearGaugeChartComponent,
    PieChartComponent,
    DataTableComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    BrowserAnimationsModule,
    NgxChartsModule,
    MatSidenavModule,
    MatButtonModule,
    MatIconModule,
    MatToolbarModule,
    LayoutModule,
    MatListModule,
    MatTableModule,
    MatPaginatorModule,
    AgmCoreModule.forRoot({apiKey: 'AIzaSyBfjgrSMIQqJWa6ns58QPHEG2-Q6uiicIU'})

  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}
