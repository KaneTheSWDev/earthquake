import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {VerticalBarChartComponent} from './vertical-bar-chart/vertical-bar-chart.component';
import {LinearGaugeChartComponent} from './linear-gauge-chart/linear-gauge-chart.component';
import {PieChartComponent} from './pie-chart/pie-chart.component';
import {MapComponent} from './map/map.component';
import {DataTableComponent} from './data-table/data-table.component';


const routes: Routes = [
  {path: 'home', pathMatch: 'full', component: LinearGaugeChartComponent},
  {path: 'vertical-chart', pathMatch: 'full', component: VerticalBarChartComponent},
  {path: 'linear-chart', pathMatch: 'full', component: LinearGaugeChartComponent},
  {path: 'pie-chart', pathMatch: 'full', component: PieChartComponent},
  {path: 'map', pathMatch: 'full', component: MapComponent},
  {path: 'table', pathMatch: 'full', component: DataTableComponent},

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
