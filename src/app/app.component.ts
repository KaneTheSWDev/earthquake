import {Component, OnDestroy, OnInit} from '@angular/core';
import {EarthquakeService} from './services/earthquake.service';
import {Subject, Subscription, interval, timer} from 'rxjs';
import {takeUntil, switchMap, tap, delay} from 'rxjs/operators';
import {EarthquakeResponse} from '../app/models/earthquakeResponse';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit, OnDestroy {
  title = 'Earthquake Data Visualization';
  data: EarthquakeResponse;
  refresh = false;
  today: Date;
  opened = false;

  private destroyed$ = new Subject();


  constructor(private readonly earthquakeService: EarthquakeService) {
  }

  ngOnInit() {
    this.earthquakeService.getEarthquakeData().pipe(
      takeUntil(this.destroyed$)
    ).subscribe(data => {
      this.data = data;
    });

    timer(0, 10000).pipe(
      takeUntil(this.destroyed$),
      tap(() => this.refresh = true),
      switchMap(() =>
        this.earthquakeService.refreshEarthquakeData())
    ).subscribe(() => {
      console.log('refreshed');
      this.refresh = false;
    });
    this.today = new Date();
  }

  onRefresh() {
    this.refresh = true;
    console.log('refreshed');
    this.earthquakeService.refreshEarthquakeData().subscribe(() => {
      this.refresh = false;
    });
  }

  ngOnDestroy() {
    this.destroyed$.next();
    this.destroyed$.complete();
  }
}

