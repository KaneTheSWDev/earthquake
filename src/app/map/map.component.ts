import {Component, OnInit} from '@angular/core';
import {EarthquakeService} from '../services/earthquake.service';
import {EarthquakeResponse} from '../models/earthquakeResponse';
import {dashCaseToCamelCase} from '@angular/compiler/src/util';


@Component({
  selector: 'app-earthquake-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.css']
})
export class MapComponent implements OnInit {
  properties: Array<any>;
  geometries: Array<any>;
  zoom = 2;
  lat = -38.416096;
  long = -63.616673;
  public mapData = [];
  data: EarthquakeResponse;


  constructor(private readonly earthquakeService: EarthquakeService) {
  }

  ngOnInit() {
    this.earthquakeService.getEarthquakeData().subscribe(data => {
      this.data = data;
      this.generateMapData();
    });
  }




  generateMapData() {
    for (const g of this.data.geometries) {
      const tempData: any = {
        latitude: g.coordinates[0],
        longitude: g.coordinates[1],
        draggable: false,
      };
      this.mapData.push(tempData);
    }
    console.log(this.mapData);

  }
}
